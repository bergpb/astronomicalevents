import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    LOGGER_LEVEL = os.getenv("LOGGER_LEVEL")

    HOST = os.getenv("DB_HOST")
    DATABASE = os.getenv("DB_DATABASE")
    USERNAME = os.getenv("DB_USERNAME")
    PASSWORD = os.getenv("DB_PASSWORD")

    API_URL = os.getenv("API_URL")

    CHAT_ID = os.getenv("CHAT_ID")
    BOT_TOKEN = os.getenv("BOT_TOKEN")
