import os
import psycopg2
from logger import logger
from config import Config
from psycopg2.extras import RealDictCursor

config = Config()


class Database:
    def open_connection(self):
        self.conn = None

        try:
            self.conn = psycopg2.connect(
                host=config.HOST,
                database=config.DATABASE,
                user=config.USERNAME,
                password=config.PASSWORD,
            )

        except (Exception, psycopg2.DatabaseError) as error:
            logger.info(f"Oops! Fail to connect with database: {type(error)}: {error}")

    def create_table_users(self):
        """Create tables in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """
                        CREATE TABLE IF NOT EXISTS users (
                            id SERIAL PRIMARY KEY,
                            chat_id VARCHAR(20) NOT NULL,
                            username VARCHAR(20),
                            firstname VARCHAR(20),
                            receive_notifications BOOL DEFAULT 'f',
                            datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                            UNIQUE (chat_id)
                        );
                    """
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(f"Oops! Fail to create table users: {type(error)}: {error}")

    def create_table_commands(self):
        """Create tables in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """
                        CREATE TABLE IF NOT EXISTS commands (
                            id SERIAL PRIMARY KEY,
                            chat_id VARCHAR(20) NOT NULL,
                            command VARCHAR(20),
                            datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                            FOREIGN KEY (chat_id) REFERENCES users(chat_id)
                        );
                    """
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(f"Oops! Fail to create table commands: {type(error)}: {error}")

    def toogle_user_notifications(self, chat_id):
        """Toogle if user choice to receive notifications or not"""
        set_value = None
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    "select receive_notifications from users where chat_id=(%s)",
                    (str(chat_id),),
                )

                receive_notifications = cur.fetchone()[0]

                if receive_notifications is False:
                    cur.execute(
                        """update users set receive_notifications = %s where chat_id = %s""",
                        (
                            True,
                            str(chat_id),
                        ),
                    )
                    set_value = True
                else:
                    cur.execute(
                        """update users set receive_notifications = %s where chat_id = %s""",
                        (
                            False,
                            str(chat_id),
                        ),
                    )
                    set_value = False

                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(f"Oops! Fail to insert user data: {type(error)}: {error}")

        return set_value

    def insert_user_info(self, data):
        """Insert data in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    "select * from users where chat_id=(%s)", (str(data["chat_id"]),)
                )

                check = cur.fetchone()

                if check is None:
                    cur.execute(
                        """insert into users (chat_id, username, firstname) values (%s, %s, %s)""",
                        (data["chat_id"], data["username"], data["firstname"]),
                    )

                cur.execute(
                    """insert into commands (chat_id, command) values(%s, %s)""",
                    (data["chat_id"], data["message"]),
                )

                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(f"Oops! Fail to insert user data: {type(error)}: {error}")

    def drop_tables(self):
        """Drop tables in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute("""DROP TABLE IF EXISTS commands;""")
                cur.execute("""DROP TABLE IF EXISTS users;""")
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(f"Oops! Fail to drop tables: {type(error)}: {error}")


if __name__ == "__main__":
    db = Database()
    db.drop_tables()
    db.create_table_users()
    db.create_table_commands()
