.PHONE: up down logs prune build-multiarch initial-config create-tables scrapy db-backup db-restore

SERVICE := ${SERVICE}
TAG     := ${TAG}

all: up logs

up:
	@docker compose -f docker-compose-dev.yml up -d;

down:
	@docker compose -f docker-compose-dev.yml down;

logs:
	@docker compose -f docker-compose-dev.yml logs -f;

prune:
	@docker container ls -f "name=astronomical_events" -q | xargs docker container rm -f;
	@docker volume rm $$(docker volume ls --filter "name=astronomical_events" -q) -f;

test:
	@echo $(SERVICE):${TAG}

multiarch:
	@echo "Make sure to create the buildx context: 'docker buildx create --use --name builder --driver=docker-container'"
	@docker buildx build --push --platform linux/amd64,linux/arm64 --tag registry.gitlab.com/bergpb/astronomicalevents/$(SERVICE):${TAG} -f $(SERVICE)/Dockerfile $(SERVICE)/;
	@docker buildx build --push --platform linux/amd64,linux/arm64 --tag registry.gitlab.com/bergpb/astronomicalevents/$(SERVICE):latest -f $(SERVICE)/Dockerfile $(SERVICE)/;

initial-config: create-tables scrapy

create-tables:
	@docker exec -it $$(docker ps -q --filter "name=astronomical_events_web") flask db-drop;
	@docker exec -it $$(docker ps -q --filter "name=astronomical_events_web") flask db-migrate;

scrapy:
	@docker exec -it $$(docker ps -q --filter "name=astronomical_events_web") flask scrapy;

db-backup:
	@docker exec -t $$(docker ps -q --filter "name=astronomical_events_db") pg_dumpall -c -U astronomyevents > ./db/dump_$$(date +%Y-%m-%d_%H_%M).sql;

db-restore:
	@cat db/dump_2023*.sql | docker exec -i $$(docker ps -q --filter "name=astronomical_events_db") psql -U astronomyevents -d astronomy_events;
