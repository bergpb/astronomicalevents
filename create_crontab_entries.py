import os
from crontab import CronTab


def create_crontab():
    cron = CronTab(user=True)

    current_path = os.getcwd()
    script_location = f"{current_path}/send_notifications.sh"
    log_file = f"{current_path}/astronomical_events.log"

    send_daily = f"{script_location} -t daily >> {log_file} 2>&1"
    job1 = cron.new(command=send_daily, comment="aeb send notifications daily")
    job1.hour.on(9)  # hour 9
    job1.minute.on(0)  # at minute 0

    send_monthly = f"{script_location} -t monthly >> {log_file} 2>&1"
    job2 = cron.new(command=send_monthly, comment="aeb send notifications monthly")
    job2.day.on(1)  # day 1
    job2.hour.on(9)  # hour 9
    job2.minute.on(0)  # at minute 0

    cron.write()
    print("Crontab scripts created!")


if __name__ == "__main__":
    create_crontab()
