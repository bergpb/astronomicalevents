# AstronomicEventsBot

Telegram bot to send next astronomical events to user

### Kubernetes deployment:
Note: Update values in secrets-app.yaml and secrets-db.yaml files before apply these manifests.

```
kubectl apply -f namespace.yaml \
    -f secrets-app.yaml \
    -f secrets-db.yaml \
    -f config.yaml \
    -f pvc.yaml \
    -f database.yaml \
    -f app.yaml \
    -f bot.yaml \
    -f notifications-daily.yaml \
    -f notifications-monthly.yaml
```

### Database backup and restore:
Backup:
```
kubectl exec -i $(kubectl get pods -n astronomicalevents | grep astronomicalevents-db | awk '{print $1}') -n astronomicalevents -- pg_dumpall -c -U astronomicalevents > dump_$(date +%Y-%m-%d_%H_%M).sql
```

Restore:
```
cat dump_2024-10-30_10_15.sql | kubectl exec -i $(kubectl get pods -n astronomicalevents | grep astronomicalevents-db | awk '{print $1}') -n astronomicalevents -- psql -U astronomicalevents -d astronomicalevents
```
