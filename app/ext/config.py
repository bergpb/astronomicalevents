import os
import sys
import logging
from importlib import import_module
from dotenv import load_dotenv

load_dotenv()


class Config:
    SECRET_KEY = os.getenv("SECRET_KEY")

    HOST = os.getenv("DB_HOST")
    DATABASE = os.getenv("DB_DATABASE")
    USERNAME = os.getenv("DB_USERNAME")
    PASSWORD = os.getenv("DB_PASSWORD")

    CHAT_ID = os.getenv("CHAT_ID")
    BOT_TOKEN = os.getenv("BOT_TOKEN")

    JSON_SORT_KEYS = False

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    EXTENSIONS = [
        'ext.cli',
        'blueprints.api',
        'blueprints.web',
        'blueprints.web.events',
        'blueprints.web.notifications',
        'blueprints.web.users'
    ]

    ITENS_PER_PAGE = os.getenv("ITENS_PER_PAGE", 20)

    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler(sys.stdout))


def load_extensions(app):
    for extension in app.config['EXTENSIONS']:
        ext = import_module(extension)
        ext.init_app(app)
