
def format_datetime(value):
    if value != None:
        return value.strftime('%Y-%m-%d %H:%M')
    else:
        return '-'
