from flask import jsonify
from datetime import datetime as dt


def return_number_of_month(long_month_name):
    return dt.strptime(long_month_name, "%B").month


def make_response(success=True, message="", content="", response_code=200):
    """Generate a response into API

    Args:
        success (bool, optional): response returns true or false. Defaults to True.
        message (str, optional): Return message inside json. Defaults to "".
        content (str, optional): Content from json. Defaults to "".
        response_code (int, optional): Response code. Defaults to 200.

    Returns:
        json: Returns a json with arguments
    """
    return (
        jsonify(
            {
                "success": success,
                "message": message,
                "content": content,
            }
        ),
        response_code,
    )


def return_image(image_class):
    data = {
        "b1": "astrocal01.jpg",
        "b2": "astrocal02.jpg",
        "b3": "astrocal03.jpg",
        "b4": "astrocal04.jpg",
        "b5": "astrocal05.jpg",
        "b6": "astrocal06.jpg",
        "b7": "astrocal07.jpg",
        "b8": "astrocal08.jpg",
        "b9": "astrocal09.jpg",
        "b10": "astrocal10.jpg",
        "b11": "astrocal11.jpg",
        "b12": "astrocal12.jpg",
        "b13": "astrocal13.png",
        "b14": "astrocal14.jpg",
        "b15": "astrocal15.jpg",
        "b16": "astrocal16.jpg",
    }

    return data[image_class]

def return_months():
    return [
        {"number": 1, "name": "Jan"},
        {"number": 2, "name": "Fev"},
        {"number": 3, "name": "Mar"},
        {"number": 4, "name": "Apr"},
        {"number": 5, "name": "May"},
        {"number": 6, "name": "Jun"},
        {"number": 7, "name": "Jul"},
        {"number": 8, "name": "Aug"},
        {"number": 9, "name": "Sep"},
        {"number": 10, "name": "Oct"},
        {"number": 11, "name": "Nov"},
        {"number": 12, "name": "Dez"},
    ]
