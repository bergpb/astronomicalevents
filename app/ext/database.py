import os
import sys
import psycopg2
from ext.config import Config
from psycopg2.extras import RealDictCursor
from datetime import datetime as dt

config = Config()


class Database:
    def open_connection(self):
        self.conn = None

        try:
            self.conn = psycopg2.connect(
                host=config.HOST,
                database=config.DATABASE,
                user=config.USERNAME,
                password=config.PASSWORD,
            )

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            sys.exit(1)

    def create_table_events(self):
        """Create table events in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """CREATE TABLE IF NOT EXISTS events (
                        id SERIAL PRIMARY KEY,
                        title varchar(100) NOT NULL,
                        description TEXT NOT NULL,
                        image VARCHAR(80) NOT NULL,
                        dates SMALLINT[] NOT NULL,
                        month SMALLINT NOT NULL,
                        year SMALLINT NOT NULL,
                        datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                    );"""
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Exception ocurred: {type(error)}: {error}")
            sys.exit(1)

    def return_events_by_day(self, day, month, year):
        """Select events filtering by day"""
        self.open_connection()
        with self.conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute(
                """SELECT id, title, description, image, dates, month, year
                FROM events
                WHERE %s = ANY (dates)
                AND month = %s
                AND year = %s;""",
                (
                    day,
                    month,
                    year,
                ),
            )
            res = cur.fetchall()
        self.conn.close()
        return res

    def return_all_events_in_month(self, month, year):
        """Select events filtering by month"""
        self.open_connection()
        with self.conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute(
                """SELECT id, title, dates, month, year
                FROM events
                WHERE month = %s
                AND year = %s;""",
                (
                    month,
                    year,
                ),
            )
            res = cur.fetchall()
        self.conn.close()
        return res

    def count_users(self):
        """Count all events"""
        self.open_connection()
        with self.conn.cursor() as cur:
            cur.execute(
                """SELECT count(*) FROM users"""
            )
            res = cur.fetchone()
        self.conn.close()
        return res[0]

    def return_all_users(self, limit, offset):
        """Select all users"""
        self.open_connection()
        with self.conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute(
                """
                    SELECT id, chat_id, username, firstname, receive_notifications, datetime
                    FROM users
                    ORDER BY id DESC
                    LIMIT %s
                    OFFSET %s
                """, (limit, offset)
            )
            res = cur.fetchall()
        self.conn.close()
        return res

    def insert_data(self, data):
        """Insert events data in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                query = "INSERT INTO events (title, description, image, dates, month, year) VALUES (%s, %s, %s, %s, %s, %s);"
                cur.executemany(query, data)
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Exception ocurred: {type(error)}: {error}")
            sys.exit(1)

    """Functions related to Notifications table"""
    def count_notifications(self):
        """Count all events"""
        self.open_connection()
        with self.conn.cursor() as cur:
            cur.execute(
                """SELECT count(*) FROM notifications"""
            )
            res = cur.fetchone()
        self.conn.close()
        return res[0]

    def return_all_notifications(self, limit, offset):
        """Select all notifications"""
        self.open_connection()
        with self.conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute(
                """
                    SELECT id, content, is_sended, sended_at, created_at, updated_at
                    FROM notifications
                    ORDER BY id
                    LIMIT %s
                    OFFSET %s
                """, (limit, offset)
            )
            res = cur.fetchall()
        self.conn.close()
        return res

    def select_notification_message(self, notification_id):
        """Select notifications"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """SELECT content from notifications WHERE id = %s""",
                    (notification_id,),
                )
                res = cur.fetchone()
            self.conn.close()
            return res

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def insert_notifications_message(self, content):
        """Insert notification that will be send with CronJob"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """INSERT INTO notifications (content) VALUES(%s)""",
                    (content,),
                )

                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def update_notification_message(self, content, notification_id):
        """Update notification that will be send with CronJob"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """UPDATE notifications set content = %s, updated_at = %s WHERE id = %s""",
                    (content, dt.now(), notification_id,),
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def remove_notification(self, notification_id):
        """Remove notification"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """DELETE FROM notifications WHERE id = %s""",
                    (notification_id,),
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    """Functions related to Events table"""
    def count_events(self):
        """Count all events"""
        self.open_connection()
        with self.conn.cursor() as cur:
            cur.execute(
                """SELECT count(*) FROM events"""
            )
            res = cur.fetchone()
        self.conn.close()
        return res[0]

    def return_all_events(self, limit, offset):
        """Select all events"""
        self.open_connection()
        with self.conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute(
                """
                    SELECT id, title, description, image, dates, month, year
                    FROM events
                    ORDER BY id DESC
                    LIMIT %s
                    OFFSET %s
                """, (limit, offset)
            )
            res = cur.fetchall()
        self.conn.close()
        return res

    def select_event(self, event_id):
        """Select event"""
        self.open_connection()
        try:
            with self.conn.cursor(cursor_factory=RealDictCursor) as cur:
                cur.execute(
                    """
                        SELECT title, description, image, dates, month, year
                        FROM events
                        WHERE id = %s
                    """,
                    (event_id,),
                )
                res = cur.fetchone()
            self.conn.close()
            return res

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def insert_event(self, data):
        """Insert event to be send in database"""
        self.open_connection()
        dates = [int(i) for i in data["dates"].split(",")]
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """
                        INSERT INTO events (title, description, image, dates, month, year)
                        VALUES(%s, %s, %s, %s, %s, %s)
                    """,
                    (data["title"], data["description"], data["image"], dates, data["month"], data["year"],),
                )

                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def update_event(self, data, event_id):
        """Update event"""
        self.open_connection()
        dates = [int(i) for i in data["dates"].split(",")]
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """
                        UPDATE events
                        SET title = %s, description = %s, image = %s, dates = %s, month = %s, year = %s
                        WHERE id = %s
                    """,
                    (data["title"], data["description"], data["image"], dates, data["month"], data["year"], event_id,),
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def remove_event(self, event_id):
        """Remove event"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """DELETE FROM events WHERE id = %s""",
                    (event_id,),
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Fail to insert message: {type(error)}: {error}")
            sys.exit(1)

    def get_bot_statistics(self):
        """
            Check informations from bot, like all users, all users when permit to
            receive notifications, and most used commands
        """
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute("SELECT count(*) FROM events")
                count_all_events = cur.fetchone()[0]

            with self.conn.cursor() as cur:
                cur.execute("SELECT count(*) FROM users")
                count_all_users = cur.fetchone()[0]

            with self.conn.cursor() as cur:
                cur.execute(
                    "SELECT count(*) FROM users WHERE receive_notifications = 't'"
                )
                count_users_with_notifications_enabled = cur.fetchone()[0]

            with self.conn.cursor() as cur:
                cur.execute("SELECT count(*) FROM commands")
                count_all_commands = cur.fetchone()[0]

            data = {
                "count_all_events": count_all_events,
                "count_all_users": count_all_users,
                "count_all_commands": count_all_commands,
                "count_users_with_notifications_enabled": count_users_with_notifications_enabled,
            }

            self.conn.close()
            return data

        except Exception as error:
            print(f"Oops! Fail to get bot statistics: {type(error)}: {error}")
            sys.exit(1)

    def drop_table(self):
        """Drop events table in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute("""DROP TABLE IF EXISTS events;""")
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            print(f"Oops! Exception ocurred: {type(error)}: {error}")
            sys.exit(1)
