from flask import request
from ext.utils import make_response
from ext.database import Database as DB
from datetime import datetime as dt

from . import api

db = DB()


@api.route("/events/filter_by", methods=["GET"])
def get_by_day():
    """Return events by day or month parameters"""
    day = request.args.get("day") if request.args.get("day") else None
    month = request.args.get("month") if request.args.get("month") else None

    year = dt.now().year

    if not day and not month:
        return make_response(False, "error", "Cannot get day or month parameter.", 400)

    if day:
        month = str(dt.now().month)
        results = db.return_events_by_day(day, month, year)

        return make_response(True, content=results)

    if month:
        results = db.return_all_events_in_month(month, year)

        return make_response(True, content=results)


@api.route("/users/statistics", methods=["GET"])
def get_users_statistics():
    """Returns bot users statistics"""
    results = db.get_bot_statistics()

    return make_response(True, content=results)
