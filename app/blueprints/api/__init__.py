from flask import Blueprint

api = Blueprint("api", __name__, url_prefix="/api/v1")

from . import views

def init_app(app):
    app.register_blueprint(api)