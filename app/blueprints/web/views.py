from flask import render_template, redirect, url_for
from ext.database import Database as DB

db = DB()

from . import web


@web.get("/")
def index():
    return redirect(url_for("web.admin"))


@web.get("/admin")
def admin():
    statistics = db.get_bot_statistics()
    return render_template("home.html", statistics=statistics)
