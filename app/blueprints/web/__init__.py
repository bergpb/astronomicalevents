from flask import Blueprint

web = Blueprint(
    "web",
    __name__,
    static_folder="static",
    template_folder="templates",
    static_url_path='/static/web',
    url_prefix="/"
)

from . import views

def init_app(app):
    app.register_blueprint(web)