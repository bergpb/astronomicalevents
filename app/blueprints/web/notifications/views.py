from flask import render_template, redirect, url_for, request, current_app, flash
from ext.database import Database as DB
from ext.config import Config
from flask_paginate import Pagination, get_page_parameter

from . import notifications

db = DB()
config = Config()


@notifications.get("/notifications")
def list():
    limit = int(config.ITENS_PER_PAGE)
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (page*limit) - limit

    notifications_total = db.count_notifications()
    notifications = db.return_all_notifications(limit, offset)

    pagination = Pagination(page=page, page_per=limit, total=notifications_total)

    return render_template(
        "notifications/list.html",
        pagination=pagination,
        notifications=notifications
    )


@notifications.route("/notifications/new", methods=["GET", "POST"])
def new():
    if request.method == "POST":
        content = request.form['content']
        if not content:
            flash("Check form data!", "danger")
        else:
            try:
                db.insert_notifications_message(content)
                return redirect(url_for("notifications.list"))
            except Exception as err:
                flash("Fail on save form data!", "danger")
                current_app.logger.error(err)

    return render_template("notifications/new.html")


@notifications.route("/notifications/edit/<int:notification_id>", methods=["GET", "POST"])
def edit(notification_id):
    notification = db.select_notification_message(notification_id)[0]

    if request.method == "POST":
        content = request.form['content']
        if not content:
            flash("Check form data!", "danger")
        else:
            try:
                content = request.form.get('content')
                db.update_notification_message(content, notification_id)
                return redirect(url_for("notifications.list"))
            except Exception as err:
                flash("Fail on save form data!", "danger")
                current_app.logger.error(err)

    return render_template("notifications/edit.html", content=notification)

@notifications.get("/notifications/delete/<int:notification_id>")
def delete(notification_id):
    try:
        db.remove_notification(notification_id)
    except Exception as err:
        flash("Fail on save form data!", "danger")
        current_app.logger.error(err)

    return redirect(url_for("notifications.list"))
