from flask import render_template, redirect, url_for, request, current_app, flash
from ext.database import Database as DB
from ext.utils import return_months, return_image
from ext.config import Config
from flask_paginate import Pagination, get_page_parameter

from . import events

db = DB()
config = Config()


@events.get("/events")
def list():
    limit = int(config.ITENS_PER_PAGE)
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (page*limit) - limit

    events_total = db.count_events()
    events = db.return_all_events(limit, offset)

    pagination = Pagination(page=page, page_per=limit, total=events_total)

    return render_template(
        "events/list.html",
        pagination=pagination,
        events=events
    )


@events.route("/events/new", methods=["GET", "POST"])
def new():
    months = return_months()
    if request.method == "POST":
        title = request.form['title']
        description = request.form['description']
        image = request.form['image']
        dates = request.form['dates']
        month = request.form['month']
        year = request.form['year']

        if not title or not description or not image or not dates or not month or not year:
            flash("Check form data!", "danger")
        else:
            try:
                data = {
                    "title": title,
                    "description": description,
                    "image": image,
                    "dates": dates,
                    "month": month,
                    "year": year
                }
                db.insert_event(data)
                return redirect(url_for("events.list"))
            except Exception as err:
                flash("Fail on save form data!", "danger")
                current_app.logger.error(err)

    return render_template(
        "events/new.html",
        months=months
    )


@events.route("/events/edit/<int:event_id>", methods=["GET", "POST"])
def edit(event_id):
    event = db.select_event(event_id)
    months = return_months()

    if request.method == "POST":
        title = request.form['title']
        description = request.form['description']
        image = request.form['image']
        dates = request.form['dates']
        month = request.form['month']
        year = request.form['year']

        if not title or not description or not image or not dates or not month or not year:
            flash("Check form data!", "danger")
        else:
            try:
                data = {
                    "title": title,
                    "description": description,
                    "image": image,
                    "dates": dates,
                    "month": month,
                    "year": year
                }
                db.update_event(data, event_id)
                return redirect(url_for("events.list"))
            except Exception as err:
                flash("Fail on save form data!", "danger")
                current_app.logger.error(err)

    return render_template(
        "events/edit.html",
        event=event,
        months=months
    )


@events.get("/events/delete/<int:event_id>")
def delete(event_id):
    try:
        db.remove_event(event_id)
    except Exception as err:
        flash("Fail to remove event", "danger")
        current_app.logger.error(err)

    return redirect(url_for("events.list"))
