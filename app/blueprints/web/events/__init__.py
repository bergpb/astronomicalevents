from flask import Blueprint

events = Blueprint(
    "events",
    __name__,
    static_folder="static",
    static_url_path='/static/web',
    url_prefix="/admin"
)

from . import views

def init_app(app):
    app.register_blueprint(events)