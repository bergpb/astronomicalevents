from ext.filters import format_datetime
from ext.config import Config, load_extensions
from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension

toolbar = DebugToolbarExtension()


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    app.jinja_env.filters["format_datetime"] = format_datetime

    toolbar.init_app(app)

    from ext.cli import init_app

    init_app(app)

    load_extensions(app)

    return app
