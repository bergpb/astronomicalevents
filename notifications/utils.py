import requests
from logger import logger
from config import Config
from database import Database
from calendar import month_name
from datetime import datetime as dt
from collections import namedtuple
from time import sleep


config = Config()
db = Database()

NL = ".\n"
BOT_TOKEN = config.BOT_TOKEN
URL_SEASKY = f"http://www.seasky.org/astronomy/astronomy-calendar-{dt.now().year}.html"
Content = namedtuple("Content", "content")

users = db.get_all_users()

def send_daily_messages():
    """Send bot messages into users"""
    daily_messages = db.get_daily_messages()

    if len(daily_messages) > 0:
        send_notifications(daily_messages, users, "daily messages", True)
    else:
        logger.info("We don't have daily messages to send today!")


def send_daily_events():
    """Send today events"""
    content_body = ""
    day = dt.now().day
    month = dt.now().month
    year = dt.now().year
    events_today = db.return_all_events_for_day(day, month, year)

    if len(events_today) > 0:
        for item in events_today:
            content_body += format_message_day(item)

        content = [
            Content(
                f"""*✨Astronomical Events✨*\n{content_body}\n🔗 [From: Seasky]({URL_SEASKY})"""
            )
        ]

        send_notifications(content, users, "daily")

    else:
        logger.info("We don't have daily events to send today!")


def send_monthly_events():
    """Send events in this month"""
    content_body = ""
    month = dt.now().month
    year = dt.now().year
    month_name = return_name_of_month(month)
    monthly_messages = db.return_all_events_for_month(month, year)

    if len(monthly_messages) > 0:
        for item in monthly_messages:
            content_body += format_message_month(item)

        content = [
            Content(
                f"""*✨Astronomical Events✨*\n
📅 *{month_name} Events*\n
{content_body}🔗 [From: Seasky]({URL_SEASKY})
            """
            )
        ]

        send_notifications(content, users, "monthly")

    else:
        logger.info("We don't have monthly events to send in this month!")


def format_message_day(data):
    dates = f"{', '.join(str(item) for item in data.dates)}"
    month = return_name_of_month(dt.now().month)
    description_formatted = data.description.replace(". ", NL)

    return f"""\n📅 *{dates} of {month}*\n
*{data.title}*\n
💬 {description_formatted}\n"""


def format_message_month(data):
    dates = f"{', '.join(str(item) for item in data.dates)}"
    month = return_name_of_month(dt.now().month)

    return f"""📅 *{dates}* - *{data.title}*\n\n"""


def return_name_of_month(month_number):
    return month_name[month_number]


def send_notifications(messages, users, type, mark_with_sended=False):
    """Get messages and users to send notifications"""
    for msg in messages:
        for user in users:
            res = requests.post(
                f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage",
                {
                    "chat_id": user.chat_id,
                    "text": msg.content,
                    "parse_mode": "markdown",
                    "disable_web_page_preview": True,
                },
            )
            if res.status_code == 200:
                if mark_with_sended:
                    db.mark_notification_with_sended(msg.id, dt.now())
                logger.info(
                    f"Success to send {type} notification send to user: {user.firstname}"
                )
            else:
                res = res.json()
                logger.warning(
                    f"""Fail to send notification to user: {user.firstname}, Error: {res["error_code"]}, Message: {res["description"]}"""
                )
            sleep(2)
